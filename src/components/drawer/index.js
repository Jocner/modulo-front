import * as React from 'react';
import '../../index.css';
import { logo_black } from '../../assets/index' ;
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import CssBaseline from '@mui/material/CssBaseline';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import { MENU_LIST } from '../../consts/index'
import AssignmentInd from '@mui/icons-material/AssignmentInd';

const drawerWidth = 240;

export default function PersistentDrawerLeft( props ) {

    const { open } = props;
    const Item = MENU_LIST[0];

  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      <Drawer
        sx={{
          width: drawerWidth,
          flexShrink: 0,
          '& .MuiDrawer-paper': {
            width: drawerWidth,
            boxSizing: 'border-box',
          },
        }}
        variant="persistent"
        anchor="left"
        open={open}
      >
        <div className='header-drawer'>
            <img src={logo_black} className='img-logo' alt="" />
        </div>
        <Divider />
        <List>
            <ListItem selected={true} button key={Item}>
              <ListItemIcon>
                <AssignmentInd />
              </ListItemIcon>
              <ListItemText primary={Item} />
            </ListItem>
        </List>
      </Drawer>
    </Box>
  );
}