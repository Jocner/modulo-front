import React from "react";
import { Container } from '@mui/material';
import { CharOne } from '../../components/drawer/index';

import './index.css';

export const SummaryFollowers = () => {

  // const [campusSelected, setCampus] = useState('');
  
  // const handleChangeCampus = (event) => {
  //   setCampus(event.target.value);
  // };

  const dataCharOne = {
    labels: ['Puente Alto', 'Stgo. Centro', 'Montevideo'],
    datasets: [
      {
        label: '2021',
        data: [100, 200, 300],
        borderColor: 'rgb(255, 99, 132)',
        backgroundColor: 'rgba(255, 99, 132, 0.5)',
      },
      {
        label: '2022',
        data: [10, 240, 302],
        borderColor: 'rgb(53, 162, 235)',
        backgroundColor: 'rgba(53, 162, 235, 0.5)',
      },
    ],
  };

  return (
    <Container maxWidth="sm">
      <div className='input-register'>
        <CharOne title='Nuevos convertidos' data={dataCharOne} />
      </div>
    </Container>
  );
};