import { React, Fragment, useState } from "react";
import api from '../../Apis/api';
import axios from 'axios';
import Swal from "sweetalert2";

import Input from "@mui/material/Input";
import Box from '@mui/material/Box';
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";


const Formulario = () => {
  
  const [countFollower, setCountFollower] = useState("");
  const [typeEnvent, setTypeEnvent] = useState("");
  const [campus, setCampus] = useState("");
  const [hour, setHour] = useState("");
  const [date, setDate] = useState("");


  const leerCountFollower =(e) =>{
    setCountFollower(e.target.value);
  }

  const leerTypeEnvent = (e) => {
    setTypeEnvent(e.target.value);
  };

  const leerCampus = (e) => {
    setCampus(e.target.value);
  };

  const leerHour = (e) => {
    setHour(e.target.value);
  };

  const leerDate = (e) => {
    setDate(e.target.value);
  };

const enviarDatos = async (e) => {
  e.preventDefault();

  if(countFollower !== "" && typeEnvent !== "" && campus !== "" && hour !== "" && date !== ""){

    const data = {
      count_follower: countFollower,
      type_envent: typeEnvent,
      campus: campus,
      hour: hour,
      date: date,
    };
    await axios
      .post(api.urlBase, data)
      .then((response) => {
        console.log(response);

        Swal.fire({
          icon: "success",
          title: "¡Registro exitoso!",
          text: "Se han enviados los datos correctamente",
        });
        setCountFollower("");
        setTypeEnvent("");
        setCampus("");
        setHour("");
        setDate("");
      })
      .catch((error) => {
        console.log(error);
        Swal.fire({
          icon: "error",
          title: "Error",
          text: "Lo sentimos ha ocurrido un error",
        });
      });
  }else {
    Swal.fire({
      icon: "error",
      title: "Error",
      text: "Debes completar todos los campos",
    });
  }
  
};


  return (
    <Fragment>
      <Box
        sx={{
          flexGrow: 1,
          width: "60%",
          margin: "20px auto",
          padding: "2rem",
          alignSelf: "center",
          backgroundColor: "#f5f5f5",
          "&:hover": {
            opacity: [0.9, 0.8, 0.7],
          },
        }}
      >
        <h1>Followers Jesús</h1>
        <form className="row" onSubmit={enviarDatos}>
          <Grid
            container
            direction="row"
            justifyContent="center"
            spacing={3}
            sx={{
              margin: "0px 0px 20px 0px",
              padding: "2rem",
              alignSelf: "center",
            }}
          >
            <Grid item xs={12} md={12}>
              <FormControl variant="standard">
                <InputLabel htmlFor="formatted-text-mask-input">
                  Cantidad
                </InputLabel>
                <Input
                  type="number"
                  value={countFollower}
                  onChange={leerCountFollower}
                  name="countFollower"
                />
              </FormControl>
            </Grid>
            <Grid item xs={12} md={12}>
              <FormControl variant="standard">
                <InputLabel htmlFor="formatted-text-mask-input">
                  Tipo de Evento
                </InputLabel>
                <Input
                  type="text"
                  value={typeEnvent}
                  placeholder="Tipo de Evento"
                  onChange={leerTypeEnvent}
                  name="typeEnvent"
                />
              </FormControl>
            </Grid>
            <Grid item xs={12} md={12}>
              <FormControl>
                <InputLabel htmlFor="formatted-text-mask-input">
                  Campus
                </InputLabel>
                <Input type="text" onChange={leerCampus} name="campus"  value={campus}/>
              </FormControl>
            </Grid>
            <Grid item xs={12} md={12}>
              <FormControl>
                <InputLabel htmlFor="formatted-text-mask-input">
                  Hora
                </InputLabel>
                <Input
                  type="text"
                  value={hour}
                  className="form-control"
                  onChange={leerHour}
                  name="hour"
                />
              </FormControl>
            </Grid>
            <Grid item xs={12} md={12}>
              <FormControl>
                <Input
                  type="date"
                  label="Fecha"
                  value={date}
                  placeholder="Fecha"
                  onChange={leerDate}
                  name="date"
                  sx={{ width: 220 }}
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </FormControl>
            </Grid>
            <Grid item xs={12} md={12}>
              <Button variant="contained" type="submit">
                Enviar
              </Button>
            </Grid>
          </Grid>
        </form>
      </Box>
    </Fragment>
  );
}
export default Formulario;
